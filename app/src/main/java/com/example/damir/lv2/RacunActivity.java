package com.example.damir.lv2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class RacunActivity extends Activity implements View.OnClickListener {

    public static final String KEY_CONVERT = "convert";
    public static final String KEY_INPUT = "input";
    public static final String KEY_FROM = "from";
    public static final String KEY_TO = "to";

    public static final double USD = 0.166334;
    public static final double POUND = 2.20462262;
    public static final double FAHRENHEIT = 1.8;
    public static final double FAHRENHEIT2 = 32;
    public static final double MILE = 0.621371192;

    TextView tvTitle, tvFrom, tvTo;
    EditText etUnos;
    Button btnConvert;
    String from, to, key_id, rez;
    double rezultat;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_racun);
        this.inicijalizacija();
    }

    private void inicijalizacija(){
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        tvFrom = (TextView) findViewById(R.id.tvFrom);
        tvTo = (TextView) findViewById(R.id.tvTo);
        etUnos = (EditText) findViewById(R.id.etUnos);

        btnConvert = (Button) findViewById(R.id.btnConvert);
        this.btnConvert.setOnClickListener(this);

        Intent convertIntent = this.getIntent();
        if(convertIntent.hasExtra(MainActivity.KEY_TITLE)){
            String title = convertIntent.getStringExtra(MainActivity.KEY_TITLE);
            tvTitle.setText(title);
        }

        if(convertIntent.hasExtra(MainActivity.KEY_FROM)){
            String from = convertIntent.getStringExtra(MainActivity.KEY_FROM);
            tvFrom.setText(from);
        }

        if(convertIntent.hasExtra(MainActivity.KEY_TO)){
            String to = convertIntent.getStringExtra(MainActivity.KEY_TO);
            tvTo.setText(to);
        }

        if(convertIntent.hasExtra(MainActivity.KEY_ID)){
             key_id = convertIntent.getStringExtra(MainActivity.KEY_ID);
        }

    }


    @Override
    public void onClick(View v) {
             //int unos = Integer.parseInt(this.etUnos.getText().toString());
             String input = this.etUnos.getText().toString();
             int unos = Integer.parseInt(input);

            Intent naTreciActivity = new Intent(getApplicationContext(), ConvertActivity.class);
            switch (key_id) {
                case "1":
                    from = "Kilogram";
                    to = "Pound";
                    rezultat = unos * POUND ;
                    rez = Double.toString(rezultat);
                    naTreciActivity.putExtra(KEY_FROM, from);
                    naTreciActivity.putExtra(KEY_TO, to);
                    naTreciActivity.putExtra(KEY_CONVERT, rez);
                    naTreciActivity.putExtra(KEY_INPUT, input);

                    break;

                case "2":
                    from = "Celsius";
                    to = "Fahrenheit";
                    rezultat = unos * FAHRENHEIT + FAHRENHEIT2;
                    rez = Double.toString(rezultat);
                    naTreciActivity.putExtra(KEY_FROM, from);
                    naTreciActivity.putExtra(KEY_TO, to);
                    naTreciActivity.putExtra(KEY_CONVERT, rez);
                    naTreciActivity.putExtra(KEY_INPUT, input);
                    break;

                case "3":
                    from = "Croatian Kuna";
                    to = "American Dollar";
                    rezultat = unos * USD;
                    rez = Double.toString(rezultat);
                    naTreciActivity.putExtra(KEY_FROM, from);
                    naTreciActivity.putExtra(KEY_TO, to);
                    naTreciActivity.putExtra(KEY_CONVERT, rez);
                    naTreciActivity.putExtra(KEY_INPUT, input);
                    break;

                case "4":
                    from = "Kilometer";
                    to = "Mile";
                    rezultat = unos * MILE;
                    rez = Double.toString(rezultat);
                    naTreciActivity.putExtra(KEY_FROM, from);
                    naTreciActivity.putExtra(KEY_TO, to);
                    naTreciActivity.putExtra(KEY_CONVERT, rez);
                    naTreciActivity.putExtra(KEY_INPUT, input);
                    break;
            }

            this.startActivity(naTreciActivity);

    }
}
