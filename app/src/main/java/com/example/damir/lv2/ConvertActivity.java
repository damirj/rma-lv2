package com.example.damir.lv2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class ConvertActivity extends Activity {

    TextView tvFrom, tvUnos, tvTo, tvRezultat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_convert);
        this.inicijalizacija();
    }

    private void inicijalizacija(){
        tvFrom = (TextView) findViewById(R.id.tvFrom);
        tvTo = (TextView) findViewById(R.id.tvTo);
        tvUnos = (TextView) findViewById(R.id.tvUnos);
        tvRezultat = (TextView) findViewById(R.id.tvRezultat);


        Intent RacunIntent = this.getIntent();
        if(RacunIntent.hasExtra(RacunActivity.KEY_FROM)){
            String from = RacunIntent.getStringExtra(RacunActivity.KEY_FROM);
            tvFrom.setText(from);
        }

        if(RacunIntent.hasExtra(RacunActivity.KEY_TO)){
            String to = RacunIntent.getStringExtra(RacunActivity.KEY_TO);
            tvTo.setText(to);
        }

        if(RacunIntent.hasExtra(RacunActivity.KEY_INPUT)){
           // int unos = RacunIntent.getIntExtra("KEY_INPUT", 0);
           // tvUnos.setText(unos);
            String unos = RacunIntent.getStringExtra(RacunActivity.KEY_INPUT);
            /*int novo = Integer.parseInt(unos);
            novo = novo + 2;
            String nazad = Integer.toString(novo);
*/
            tvUnos.setText(unos);
        }

        if(RacunIntent.hasExtra(RacunActivity.KEY_CONVERT)){
            String rezultat = RacunIntent.getStringExtra(RacunActivity.KEY_CONVERT);
            tvRezultat.setText(rezultat);
        }
    }


}
