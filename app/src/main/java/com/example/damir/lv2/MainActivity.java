package com.example.damir.lv2;

import android.app.Activity;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends Activity implements View.OnClickListener {

    public static final String KEY_TITLE = "title";
    public static final String KEY_FROM = "from";
    public static final String KEY_TO = "to";
    public static final String KEY_ID = "id";
    public static final String ID1 = "1";
    public static final String ID2 = "2";
    public static final String ID3 = "3";
    public static final String ID4 = "4";




    ImageView ivWeight, ivTemp, ivMoney, ivDistance;

    String title, from, to;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.inicijalizacija();
    }

    private void inicijalizacija(){
        ivWeight = (ImageView) findViewById(R.id.ivWeight);
        ivTemp = (ImageView) findViewById(R.id.ivTemp);
        ivMoney = (ImageView) findViewById(R.id.ivMoney);
        ivDistance = (ImageView) findViewById(R.id.ivDistance);

        this.ivWeight.setOnClickListener(this);
        this.ivTemp.setOnClickListener(this);
        this.ivMoney.setOnClickListener(this);
        this.ivDistance.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent naDrugiActivity = new Intent(getApplicationContext(), RacunActivity.class);
        switch (v.getId()){
            case (R.id.ivWeight):
                title = "Weight Conversion";
                from = "Kilogram";
                to = "Pound";
                naDrugiActivity.putExtra(KEY_TITLE, title);
                naDrugiActivity.putExtra(KEY_FROM, from);
                naDrugiActivity.putExtra(KEY_TO, to);
                naDrugiActivity.putExtra(KEY_ID, ID1);
                break;

            case (R.id.ivTemp):
                title = "Temperature Conversion";
                from = "Celsius";
                to = "Fahrenheit";
                naDrugiActivity.putExtra(KEY_TITLE, title);
                naDrugiActivity.putExtra(KEY_FROM, from);
                naDrugiActivity.putExtra(KEY_TO, to);
                naDrugiActivity.putExtra(KEY_ID, ID2);
                break;

            case (R.id.ivMoney):
                title = "Money Conversion";
                from = "Croatian Kuna";
                to = "American Dollar";
                naDrugiActivity.putExtra(KEY_TITLE, title);
                naDrugiActivity.putExtra(KEY_FROM, from);
                naDrugiActivity.putExtra(KEY_TO, to);
                naDrugiActivity.putExtra(KEY_ID, ID3);
                break;

            case (R.id.ivDistance):
                title = "Distance Conversion";
                from = "Kilometer";
                to = "Mile";
                naDrugiActivity.putExtra(KEY_TITLE, title);
                naDrugiActivity.putExtra(KEY_FROM, from);
                naDrugiActivity.putExtra(KEY_TO, to);
                naDrugiActivity.putExtra(KEY_ID, ID4);
                break;
        }
        this.startActivity(naDrugiActivity);
    }
}
